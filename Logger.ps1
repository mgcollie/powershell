<#
.SYNOPSIS
A versatile logging class supporting varied log levels: DEBUG, INFO, WARNING, ERROR, and CRITICAL.

.DESCRIPTION
The Logger class provides a simple interface for logging messages at different severity levels. It offers
dedicated methods for each log level and allows the setting of an individual log level per instance. This
design means the log level is specific to each Logger instance, enabling more granular control over logging
output without affecting other instances.

.PARAMETER Name
Specifies the name of the logger. This identifier is included in each log message, aiding in tracing the
source of the message.

.PARAMETER LogLevel
(Optional) Sets the logging level for the current Logger instance. Acceptable values are: DEBUG, INFO,
WARNING, ERROR, and CRITICAL. If not specified, the default level is "INFO". This level determines the
minimum severity of messages that will be logged by this instance, filtering out messages below this threshold.

.EXAMPLE
# Instantiate a logger with the default log level (INFO):
$logger = [Logger]::new('MyLogger')

# Instantiate a logger and set the log level to DEBUG for this instance:
$logger = [Logger]::new('MyLogger', 'DEBUG')

# Logging messages at various levels:
$logger.info('This is an information message.')
$logger.debug('This is a debug message.')

# Adjust the log level to WARNING for this logger instance:
$logger.SetLevel('WARNING')

.NOTES
This class requires PowerShell 5.0 or later, due to its use of class definitions.
Packaging this class within a PowerShell module is recommended for enhanced modularity and maintainability.
This approach allows for easy reuse across different projects and streamlines updates.


.LINK
https://gitlab.com/mgcollie/powershell/Logger.ps1 - Source code repository for the Logger class.

.LINK
https://docs.microsoft.com/powershell/scripting/overview - PowerShell scripting overview and concepts.

.LINK
Get-Help about_Functions - Detailed information on PowerShell functions.

.LINK
https://docs.microsoft.com/powershell/module/microsoft.powershell.utility/start-process - Learn how to open links in the default web browser using Start-Process.
#>
class Logger {
    [hashtable]$loglevels = @{
        "DEBUG" = 10;
        "INFO" = 20;
        "WARNING" = 30;
        "ERROR" = 40;
        "CRITICAL" = 50;
    }

    [string]$loglevel = "INFO"
    [string]$name

    Logger([string]$name, [string]$logLevel = "INFO") {
        $this.name = $name
        if ($logLevel -in $this.loglevels.Keys) {
            $this.loglevel = $logLevel
        } else {
            $validLevels = ($this.loglevels.Keys -join ", ")
            throw "Invalid Log Level: $logLevel. Valid levels are: $validLevels."
        }
    }

    [void]SetLevel([string]$Level) {
        # Convert $Level to uppercase for case-insensitive comparison
        $Level = $Level.ToUpper()
        if ($Level -in $this.loglevels.Keys) {
            $this.loglevel = $Level
        } else {
            $validLevels = ($this.loglevels.Keys -join ", ")
            throw "Invalid Log Level: $Level. Valid levels are: $validLevels."
        }
    }

    hidden [void]writeLog([string]$Message, [string]$Level) {
        $timestamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
        $logEntry = "[$timestamp] [$Level] [$($this.name)] $Message"

        $messageLevel = $this.loglevels[$Level]
        $numericlevels = $this.loglevels[$this.loglevel]

        if ($messageLevel -ge $numericlevels) {
            $foregroundColor = switch ($Level) {
                "DEBUG" { "Green" }
                "INFO" { "Gray" }
                "WARNING" { "Yellow" }
                "ERROR" { "Red" }
                "CRITICAL" { "Magenta" }
                default { "Gray" }
            }

            # Log to console with color
            Write-Host $logEntry -ForegroundColor $foregroundColor
        }
    }

    [void]debug([string]$Message) { $this.writeLog($Message, "DEBUG") }
    [void]info([string]$Message) { $this.writeLog($Message, "INFO") }
    [void]warning([string]$Message) { $this.writeLog($Message, "WARNING") }
    [void]error([string]$Message) { $this.writeLog($Message, "ERROR") }
    [void]critical([string]$Message) { $this.writeLog($Message, "CRITICAL") }
}